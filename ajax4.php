<?php
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $json = file_get_contents( __DIR__ . DIRECTORY_SEPARATOR . 'students.json' ); // в примере все файлы в корне
    $data = json_decode($json, true);
    echo json_encode($data);

    die;
}